# masters-thesis

My master of science thesis titeled "Procedural generation of road networks using an L-system".

The thesis is written in LaTeX.

Copyright 2013, Martin Jormedal


## Generating a PDF from the LaTeX report
Required LaTeX packages to compile document on Ubuntu Linux:
(last tested on Ubuntu 19.10)
- texlive
- texlive-latex-extra
- texlive-lang-european (because the document contains text in Swedish)
- latex-cjk-common (because the document contains text in Japanese)
- latex-cjk-japanese-wadalab (for the font used for Japanese text)

To compile the document and generate a PDF execute the following command from the `Doc` subdirectory:

`pdflatex 00_main.tex`

pdflatex needs to process the document twice to generate a table of contents so it's suggested to run the above command two times.