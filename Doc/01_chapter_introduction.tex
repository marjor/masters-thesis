\chapter{Introduction}
\label{chap:introduction}


\section{A word on procedural generation}
\label{sec:introduction1}


This section is intended to give a quick introduction to the concept of procedural generation, if you are already familiar with the topic please feel free to skip forward to section \ref{sec:introduction2}.\\

Procedural generation rather obviously implies generating something by means of a procedural algorithm, as opposed to doing the same work by hand. The result can be pretty much anything from music to images to animation data to 3D models.\\

As the reader is might be aware of at this point, the project described in this paper was conducted at the gaming studio From Software in Tokyo, Japan, and while methods for procedural generation have uses in a range of fields, our focus will remain on it's role in content creation in the field of game design.\\

There are a few different motivational factors behind choosing the procedural approach but any combination of the following tend to be the main ones: 
\begin{itemize}
	\item Saving on storage space. 
	\item Saving on manual labour. 
	\item The possibility of generating a better result than can generally be achieved by hand.
\end{itemize}

Saving on space was of great concern during the early days of computer games and methods of procedural generation found some of their earliest practical uses here to generate, for example, levels on the fly from a small set of resources. Saving on manual labour has constantly been the main factor behind the use of procedural generation and continues to be so. The project described in this paper also finds itself squarely in this segment. The possibility of achieving a better result than by doing something by hand has arisen fairly recently as computing power has reached new heights. Concentrating on the subject of procedural generation of 3D models (a.k.a. procedural modelling) we can conclude this is well exemplified in the highly successful work that has been done in the field of plant and tree generation, much due to the similarities between actual naturally occurring shapes of plants and fractal patterns. Fractal based algorithms are often employed in procedural modelling and such is also the case with the work described in this paper.\\

Practical use of procedural generation as well as research in the field has experienced quite a renaissance since the mid 2000's, mainly due to the increased complexity of environments and ever increasing demands of realism in computer games and computer animated films. If we limit ourselves to the world of computer games, areas where procedural methods are used successfully, or are making inroads, include texture generation, animation and 3D model generation. When it comes to procedurally generated animation data the line between procedural generation and simulation can get a bit blurred at times as much of the computation is often done in real time.\\

Relative to the amount of work that has been done on procedural modelling of natural phenomena like plants and terrain, comparatively little work has been put into generating man made structures which tend to have far less stochastic properties making it decidedly more difficult to employ fractal algorithms and still achieve believable results.\\


\section{Introducing the problem}
\label{sec:introduction2}

As was briefly pointed out in the previous section (\ref{sec:introduction1}), procedurally modelling of man made structures presents a different set of difficulties as opposed to modelling of naturally occurring shapes and patterns. In this paper we will deal with a type of man made structure that combines both properties of a purposefully designed system and some distinct stochastic properties that have been shown to lend themselves well to procedural generation, namely road networks.
The purpose of this study was to investigate one possible practical use of procedural generation in a game development setting with the goal of implementing a system that could be used for game map prototyping and/or background scenery. The reasons for choosing the prototyping route instead of aiming for production quality results were, in part the importance of game map layout to the game mechanics in the type of games where the system is applicable, and in part due to the high demands put on graphics quality in modern games versus the limited amount of man hours available for the project. Thus the focus of the project was on developing a sound algorithm that produces believable road networks, accompanied by a flexible system to control the algorithm.\\

While focusing the project on procedural generation was decided upon from early on, the exact subject matter was decided upon after a series of interviews with programmers, producers and designers at From Software. While other ideas were also floated and considered there was a degree of consensus that rapid prototyping of urban landscapes for use as, for example, support for level design and backdrops, could prove quite useful in a multitude of scenarios.

\section{Specifying the task}
\label{sec:introduction3}
The vision for the project was initially highly, I dare say overly, ambitious and imagined the system being able to generate entire cities, buildings and all, as well as the ability to nicely render these in order to preview the result. The reality of the limited time allotted and the fact that it was a one-man-project after all, quickly brought some restraint to the scope of the project.
A set of more reasonable basic requirements on the project were decided upon:
\begin{itemize}
    \item Create a system to input easily editable environment data.
    \item Construct and implement a sound algorithm for generating a road network from the environmental data.
    \item Output the result in an intermediary format that can be refined using other 3D software.
\end{itemize}
Some of the earlier ideas that were kept on as nice-to-haves included some sort of building generation and a renderer to quickly view the result of a generation cycle. Due to time constraints these only resulted in some rudimentary work on building generation.\\\\
The above stated requirements were however fulfilled to a point that was deemed acceptable.\\\\ 

This project was never awarded an official name while the research was performed but to facilitate referencing to it and avoid confusion I've decided to name it ToshiGen, from the Japanese word for city "toshi" (\begin{CJK}{UTF8}{min}都市\end{CJK}) and "gen" which is a slight play on words, referring both to "GENeration" and "gen" which is a common pronunciation for the Japanese kanji pictogram for origin or source (\begin{CJK}{UTF8}{min}元\end{CJK}).

\section{Previous work}
\label{sec:introduction4}
The generation of road networks by means of procedural generation is by no means a novel idea, there has been a fair bit of research into the problem and an interesting variety of approaches.

The shape of road networks are intrinsically influenced by two strong forces that sometimes act in synergy and sometimes at odds with one and other. One is the surrounding environment, the other is the will of man. These two competing forces are also apparent in the strategies employed when attempting to synthesise these structures. On one side are the techniques that attempt to purely imitate the geometric shape of interconnected roads in an environment. On the other side are the agent-based methods that seek to emulate human planning behaviour. Hybrids have certainly been attempted and such a method was indeed considered for ToshiGen as well. The possibility of overly increasing level of complexity of the system in relation to the possible actual visual improvement gained ultimately led to such plans being abandoned.\\

Research into the different approaches also showed that while the agent-based systems such as Lechner et al CityBuilder\cite{lechner03} resulted in interesting simulations they did not necessarily produce more believable visual results.\\\\

Other methods have relied heavily on the idea of a cell as a basic component of road networks. Grid based methods \cite{greuter03} and those based on Voronoi diagrams \cite{sun02} fall under this class. While these systems succeed in capturing some of the regularity found in road networks well, they do tend to produce results that are both overly uniform and rather simplistic in their design when applied to levels in the road hierarchy above a set of city blocks. Another problem in using what is essentially a loop as a basic building block is the lack of dead ends and the singular roads that usually connect areas of higher population density.\\\\

Finally we have the fractal pattern method, primarily represented by L-systems, a method originally designed to model the branch and root structures of plants. L-systems have proved to be able to modify in a manner that produces road networks with very believable visual results. As the visual representation rather than correct simulation of an urban environment was the goal of the project the L-system based method was selected for the project which is the subject matter of this paper. L-systems are explained in further detail in chapter \ref{chap:theory} on the theory behind this project.\\
The work that has been the greatest influence on this project is that of Pascal Müller \cite{muller01} who has made a great effort of analysing the L-system approach and implementing practical solutions based on it.\\


% Local Variables:
% TeX-master: "00_main.tex"
% End:
