\chapter{Discussion}
\label{chap:discussion}
Any piece of software is inherently a product of decisions made during it's development, both functional and non-functional, good and bad. In this section we will take a closer look at some of these decisions and the effects they had on the end result of the project. In the light of this we will also discuss what could have been done better and how the system might be improved if it were be continued.

\section{Design choices and limitations}
\label{sec:discussion1}
A project like this is doomed to have it's scope limited by the available resources, and certain problems were simply too complex to overcome within the scope of the project. As always when designing software both good and less good choices were also made in the process. I would like to dedicate this section to reflection upon some of these problems and choices with a focus on the limitations they impose and what might have been done better. 

\subsection{Usability}
\label{sec:implementation11}
One big, or rather \emph{the} big challenge with procedural generation systems is achieving balance between complexity and control. As the systems one tries to generate increase in complexity it inevitably becomes more difficult to leverage control over them in an intuitive way. By way of extension this causes problems with usability for any tool based around these methods.\\
This became quite apparent during the course of this project and it is also where I feel that ToshiGen falters the most. While the algorithm is sound and the structure as a whole is flexible and fairly easily extendible, to leverage control of this flexible system is quite a challenge. It can be a time consuming task of interleaving XML-file edits and test generations to create good results. This regrettably has quite a negative impact on the usefulness of the software as a whole. I've chosen to place some thoughts on how this may be alleviated under section \ref{sec:discussion3} "Future work" later in this chapter.

\subsection{Intermediate output of network topology without geometry generation}
\label{sec:implementation12}
The generation pipeline as described in \ref{sec:implementation1} is fixed, That is to say there is no intermediate output of data, besides some basic logging, between reading of the input data and output of the road network geometry. This all well and good in the general case but as has been discussed elsewhere in this text (\ref{sec:implementation5}) one of the goals of the design was to have the geometry generation module easily exchangeable. Having an intermediary step with the ability to output the layout of the road network would allow for inputting the same layout into different geometry generators to find the one that produced the best result. It would also allow for the possibility to give the generated network a preliminary check before passing it through more time consuming geometry generation than the current, quite rudimentary, one.

\subsection{Separation of network types}
\label{sec:implementation13}
The system does not provide for a means to disallow different network types to interconnect. The most notable effect of this is that the rail network and road network will inevitably grow together become difficult to discern. In some cases this might be the desired effect in order to create a crossing between the to two. Mode often than not though, especially in highly populated areas, the two traffic types are kept separate and it would make sense to for example let rail block roads, which will then have to run alongside the rail until some designated point where a crossing or underpass can be generated. In order to implement this the edges will need to carry more information than they currently do. These modifications are detailed in section \ref{sec:implementation22} as suggested future work.

\subsection{Improved handling of terrain obstacles}
\label{sec:implementation14}
As suggested in section \ref{sec:implementation13} above, letting some features like parts of the network ferrying different modes of transport have an impact on how the road network is grown would yield more believable results. In the same manner this applies to certain terrain features as well. Most notably bodies of water and very steep terrain. While the system has some ability to avoid overly abrupt inclines there is currently no representation for rivers or lakes which makes avoiding them rather difficult.\\
It would however be a quite simple task to leverage the mechanism for image maps already present, as well as some gentle modification of the production rules in order to to achieve the desired effect. One could create a map with areas corresponding to the obstacles we wish to avoid when growing the network and let the production rules check against this. It would likewise be a simple task to avoid deep valleys and mountains by checking against the terrain topology map.\\\\
Unfortunately the improvements suggested above can't be described solely in XML so some modification of the actual C++ code would be needed. This incidentally brings us on to the subject matter of the next section (\ref{sec:implementation15}).

\subsection{Improved XML format}
\label{sec:implementation15}
The the handling of the XML file describing the initial environment and the parameters for the production rules as well as the structure used in the file itself would benefit greatly from some improvement. The structure of the file, as it is, is very flat and has a lot of focus on the description of the production rule parameters.\\
As an example the hypothetical solutions to both the suggested improvements described in section \ref{sec:implementation13} and section \ref{sec:implementation14} would benefit significantly from more, and better presented, information about the road network edge segments and image maps respectively.\\\\
Road edge segments would be easy to discern from each other by adding types and descriptions for the them in a similar manner done for the nodes. This would also simplify things for any geometry generator that created more complex structures as it would allow it to know the exact type and purpose of any road edge segment.\\\\
A more verbose definition of image maps would make their use more flexible and allow them do be defined dynamically in XML alone. As things stand a lot of the code pertaining to the access and function of image maps is hard coded in the production rules. Expanding on the effects the image maps may have on the system and encoding these parameters along with the map definition in the environment XML file would eliminate the need of a great deal of that hard coded behaviour. The added parameters could detail for example whether to repel growth in from some areas and/or encouraging it in others, define which node types are affected and which production rules should take a specific map into account.\\\\


\section{Future work}
\label{sec:discussion2}
Many of the limitations described in section \ref{sec:discussion1} would be good candidates for any extension of the system, but I've selected three specific tasks to feature as especially fitting for a (hypothetical) continuation of the project. The thinking behind the choices is that the tasks should represent well defined projects of a larger scope. All of these features were also discussed in some way or form during the original project.

\subsection{GUI}
\label{sec:implementation21}
As discussed in section \ref{sec:implementation11} one of the foremost obstacles to using the system is in the general usability. Manually editing the XML parameters for the production rules requires both some script language knowledge and a rather good understanding of how the system is implemented and how L-systems work in general.\\
To alleviate this a GUI would be most welcome. It could be used both to control the parameters, preview the generated content, as well as to make manual adjustments after the generation step. 

\subsection{Detection and subdivision of sectors}
\label{sec:implementation22}
This task is linked to and a direct prerequisite to the building generation described in section \ref{sec:implementation23} below, it would however be quite possible to take on as a separate task and to achieve good results might be a challenge in and of itself.\\
Quite naturally when a network is generated we also wind up with open spaces in between the roads, in order to generate buildings, parks, town squares and other features of an urban landscape these spaces need to be detected and their dimensions registered. To accommodate buildings they also need to be subdivided into plots of suitable geometry. 

\subsection{Building generation}
\label{sec:implementation23}
As mentioned in section \ref{sec:implementation22} above, in order to mesh the generation of buildings with the networks generated by ToshiGen one would first have to tackle the problem of detection and subdividing the open areas between the roads in the network. However, the generation of buildings is in and of itself a task of substantial complexity. Depending on the level of accuracy the effort could easily surpass the scope of the project described herein. It is an interesting topic and much work has already been done, alas there is still much room for improvement. It would also be a natural progression in the case of ToshiGen towards extending it to generate urban environments. Indeed there is some small pockets of code dedicated towards this goal floating around in the system that unfortunately had to be abandoned due to the scale of taking on the task with good results.

\subsection{Parallelisation of the generation algorithm}
\label{sec:implementation24}
As the network grows the amount of nodes to match with production rules and check for expansion increase exponentially which of course has a very adverse effect on performance. The L-system as it stands is also completely single-threaded. This is of course rather silly for a process that might lend itself quite well to parallelisation to be handled this way in this age of multi-core CPUs and GPGPU. In order to achieve a substantial speed-up it would be interesting to investigate how to handle this parallelisation. The obvious way to start would be to divide the area into subsections, this however may cause some difficulties when edges grow into another section and may need to interact with nodes and edges in said section at the same time as another thread is processing and modifying the topology there. Finding some nice way to pass these cases between threads might be a solution but one could also imagine some kind of dispatcher that ensures that all threads are working as far apart as possible at all times in order to avoid problems with simultaneous attempts to modify a node and/or edge.

\section{Conclusion}
\label{sec:discussion3}
While a fair few shortcomings of the system have been detailed in this particular chapter the result of the project was none the less a very flexible system that fulfilled the basic demands placed on it, and in some respects did even more. The modular nature of the pipeline allows for easy extension and the general approach taken in the L-system implementation would easily allow the system to be modified to generate other structures than road networks that share similar properties such as rivers, and given the systems ability to modify texture maps rudimentary land topology, such as valleys and gorges should not be outside the scope of possibility either.\\\\

The project was intended to be an investigative one and by all accounts the results were satisfactory to the client in question. It's failures served as lessons and it's successes as guidance to the people involved, myself in particular.


% Local Variables:
% TeX-master: "00_main.tex"
% End:
