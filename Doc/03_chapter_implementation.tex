\chapter{Implementation}
\label{chap:implementation}
The main focus of the project was to produce believable output, thus the most effort was put towards implementing a good generation algorithm while usability and polished UI was considered to be "nice to have" rather than a "must have". Thus the system in it's current form is implemented as a black box solution with input in the form of an XML-file with instructions for the generation algorithm and a set of image maps that can be used to input information such as a height field and population density. Output is rendered in the form of an output file that contains a simple mesh representing the generated road network, and if so indicated, a terrain-mesh generated from a height-field. The output file is in an XML-based format called Collada (more on Collada in section \ref{sec:implementation5}).

\section{System overview}
\label{sec:implementation1}
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{figures/system_overview.pdf}
    \caption{Overview of the system}
    \label{fig:implementation1}
\end{figure}
As seen in figure \ref{fig:implementation1} the system in itself is made up of three rather elementary units, input, generation and output. Each of these units has been awarded it's own chapter that further describe the methods and technologies used. The input data set and Collada format output is described in conjunction with the input and output units respectively. Focus is quite naturally on the generation step as that is where most of the magic happens.

\section{Input and XML module}
\label{sec:implementation3}
Input to the generation system is provided in two forms, an XML-file and an arbitrary number of image maps. These are combined into an initial environmental model that acts as an resource for the L-system to gather information from and modify as needed.\\

\subsection{XML input}
\label{sec:implementation31}
XML was chosen as the input format mainly due to two properties, it's widely used and quite verbose. These properties combine to make for a relatively easily human readable text-based input that despite the lack of a polished GUI allows for transparent control of the system. It is also easily imported and exported which allows it to act as an intermediate format between the generation system and a GUI were one be added in the future.\\
The XML file contains some general information about the target environment and also describe the production rules (production rules are explained in section \ref{sec:theory1}) that are to be used by the L-system.\\
The XML reader module was designed around an Open Source library called TinyXML.

\subsection{Image map input}
\label{sec:implementation32}
Image maps gives us a simple and effective way of providing densities and boundary information related to the environment onto which the road network is to be generated.\\
The role of the image maps are to provide extra data to be used by the production rules such as topology and population density data. These can optionally be used by the production rules to, for example, avoid steep inclines or prefer to branch off side roads within areas with higher population density.\\
Access to the image maps in the system is uniform and technically an arbitrary number of image maps can be defined for inclusion. The inclusion of an image map is specified in the XML configuration file, use of these maps however needs to be implemented programmatically which means that in reality is quite nontrivial to increase the amount of image maps actually used by the system. The rules implemented at the end of the the project described herein make use of at most a height map for topology and a map for population density.

\subsection{The initial environment model}
\label{sec:implementation33}
The environmental model is an encapsulation of the graph representing the road network, the available node types, the production rules for the L-system and additional data that support the operation of the L-system at large. Such additional data includes the seed for the random number generator, the total size of the domain that the network is allowed to grow within and the number of evolution steps, i.e. iterations, to execute. An example of how this information is encoded in the XML input file can be seen in example \ref{ex:implementation1}.
\begin{example}[Environment XML tags]
\label{ex:implementation1}
    The initial environment information is provided with the XML input file as shown below.
    
    \ttfamily
    \noindent<RandomSeed value="69584251" />\\
	<DomainSizeX value="100"/>\\
	<DomainSizeZ value="100"/>\\
	<EvolveStepNum value="100"/>\\
	
	\noindent<Map2DDef>\\
		\hspace*{8mm}<Semantics value="Terrain"/>\\
		\hspace*{8mm}<Path value="../../Resource/heightmap-51x51.tga"/>\\
		\hspace*{8mm}<Scale value="0.02"/>\\
	</Map2DDef>\\

	\noindent<Map2DDef>\\
		\hspace*{8mm}<Semantics value="Population"/>\\
		\hspace*{8mm}<Path value="../../Resource/popmap-512x512.tga"/>\\
		\hspace*{8mm}<Scale value="100"/>\\
	</Map2DDef>\\

	\noindent<AxiomNode>\\
		\hspace*{8mm}<Type value="MainRoadLeaf"/>\\
		\hspace*{8mm}<Pos valueX="30.1" valueY="20.1"/>\\
		\hspace*{8mm}<Width value="0.05"/>\\
	</AxiomNode>\\
\end{example}

\section{Generation, the graph-based L-system}
\label{sec:implementation4}

\subsection{Initial challenges}
\label{sec:implementation41}
The first attempts at implementing an L-system to drive the generation process resulted in a system highly based around the idea of the classic representation of a string of instructions to a turtle system. The string was implemented as a double-linked list of literals that were replaced according to the production rules of the system. As indicated in section \ref{sec:theory6} this representation presents some great difficulties when it comes to intersecting segments and the crossings they create. This was a hard learned lesson that cost a fair deal of time and rewriting of code.

\subsection{The graph based L-system}
\label{sec:implementation42}
Some solution was needed to solve the problems described in the previous section (\ref{sec:implementation41}). As described in the theory section (\ref{sec:theory6}) the solution was to be found in representing the state of the L-system after each evolution step as a graph. The literals were replaced with nodes of different types and just as with literals the production rules were constructed to replace a node for another or a set of others. The need for a turtle system to generate a visual representation of the resulting structure was eliminated by also storing the edges between connected nodes as they were created.\\\\
The nodes are also a very practical place to store the hierarchy of the network. Thus the type label of each node is used to designate it's position in said hierarchy (main road, street, alley, etc.). These type labels are defined dynamically in the XML environment specification as illustrated in example \ref{ex:implementation2}, technically allowing for an arbitrary hierarchy depth. This also provided the possibility to fulfil another requested feature. This feature was the generation of "rail" structures. The initial source of this request was that much of Tokyos urban planning and transportation, to a degree not commonly seen in other cities, is centred around the train systems and there was a wish to be able to represent structures intended for different modes of transport than automobiles. The dynamic node type labelling feature allows for the classification of nodes as rail with specific rules to grow a rail network, the possibility to create dedicated footpaths or bicycle routes that all can be marked as such and treated separately post generation is provided analogy.\\\\
In the nomenclature used in example \ref{ex:implementation2} "leaf" is used to denote nodes at the fringe of the network. These are thus freshly generated and are only connected to one edge. As they are matched against a production rule that targets their specific type label they will be changed into a "knot" if the production rule is successful in generating one or more nodes and their connecting edges. A "knot" thus signifies any node in the network with two or more edges attached.\\

\begin{example}[Example of node types declared in XML]
\label{ex:implementation2}
    \ttfamily
    <RoadNodeType name="MainRoadLeaf" id="100" />\\
    <RoadNodeType name="MainRoadKnot0" id="101" />\\
    <RoadNodeType name="MainRoadKnot1" id="102" />\\
    <RoadNodeType name="MainRoadKnot\_Branched" id="103"/>\\
    <RoadNodeType name="StreetLeaf" id="200"/>\\
    <RoadNodeType name="StreetLeaf\_JustGrow" id="201"/>\\
    <RoadNodeType name="StreetKnot" id="202"/>\\
    <RoadNodeType name="StreetKnot\_Branched" id="203"/>\\
    <RoadNodeType name="AlleyLeaf" id="300"/>\\
    <RoadNodeType name="AlleyKnot" id="301"/>\\
    <RoadNodeType name="AlleyKnot\_Branched" id="302"/>\\
\end{example}

\subsection{Growing the network, the production rule model}
\label{sec:implementation43}
Each production rule in ToshiGen is made up from two distinct parts, a piece of XML data that specifies the node to target and parameters to use, perhaps more importantly though, through the "name" attribute of the "Production" XML-element it also points to the particular C++ class that contains the logic that uses the other parameters to apply a certain operation on applicable nodes of the network. \\
Example \ref{ex:implementation3} shows an example of a production rule as defined in the XML input file for the system. It points to the production class "BranchAtAngle" and contains the parameters to be used when the logic contained in that class is applied to nodes of type "MainRoadLeaf", such as which type of new nodes to spawn, the minimum and maximum length of the edge connecting the spawned node to the parent node, parameters concerning the angle of the spawned edges and the width to be used when generating geometry for the spawned edges.\\\\

Production rules go through a few steps when applied to the graph, as first touched upon in section \ref{sec:theory6}:\\
\begin{enumerate}
    \item Find a node of matching type.
    \item Check if the conditions to apply the production rule for the matched node are fulfilled.
    \item If the check above is positive, apply the rule and spawn edges and child nodes according to the parameters provided.
    \item Check that the criteria for the spawned nodes are fulfilled.
    \item If the check above is positive, add the spawned nodes and edges to the network.
    \item Change the type of the originally matched node according to the parameters provided.
\end{enumerate}
\begin{example}[Production rule in XML]
\label{ex:implementation3}
    This example details the structure of a production rule and it's most common parameters as defined in the XML input file.\\\\
    \ttfamily
    <Production name="BranchAtAngle">\\
        \hspace*{8mm}<MatchRoadType value="MainRoadLeaf"/>\\
        \hspace*{8mm}<ChangeToType value="MainRoadKnot\_Branched"/>\\
        \hspace*{8mm}<SpawnType value="MainRoadLeaf"/>\\
        \hspace*{8mm}<MaxLength value="1"/>\\
        \hspace*{8mm}<MinLength value="0.5"/>\\
        \hspace*{8mm}<Width value="0.05"/>\\
        \hspace*{8mm}<IntersectRadius value="0.1"/>\\
        \hspace*{8mm}<MaxAngle value="50"/>\\
        \hspace*{8mm}<MinAngle value="10"/>\\
        \hspace*{8mm}<MinInbetweenAngle value="45"/>\\
    </Production>
\end{example}

Step 4 above deserves some further attention. The criteria mentioned here include checks angles against other spawned nodes and edges, as well as against the orientation of the parent node. In case the tests fail here there is generally an attempt to modify the angle of one or both of the edges and their corresponding nodes to fall within the parameters.\\
There is also a check if the spawned node is out of the bounds specified for the whole network. If a spawned node were to fall outside of the bounds it is generally rejected and the node and edge will not be added to the network.\\
Finally there is a check for intersections with other edges and proximity to other nodes in the network. This check generally goes through the steps listed below.\\
\begin{enumerate}
    \item Check for intersections with other edges.
    \item Check for other nodes within the "IntersectRadius" specified in the production rule parameters.
    \item Check for other edges within the "IntersectRadius" specified in the production rule parameters.
\end{enumerate}
Should any of these tests return a positive result there are corresponding procedures to rearrange the spawned nodes and edges to maintain the integrity of the network as well as to strive for a more believable result by creating crossings where applicable.\\\\

\begin{figure}[h]
    \centering
    \includegraphics[width=9cm]{figures/intersection_check_crossing.pdf}
    \caption{Check for edge intersections}
    \label{fig:implementation2}
\end{figure}

Figure \ref{fig:implementation2} illustrates the check for intersections between a newly created edge and any pre-existing edges. This check takes precedent and should one or more such intersections be found the newly spawned node will be moved to the point of intersection closest to the parent node and the spawned edge shortened correspondingly. Thereafter the check for nearby nodes detailed below and illustrated in both \ref{fig:implementation2} and \ref{fig:implementation3} is performed. Should it yield a result the spawned node will be discarded and the spawned edge will instead be connected to the closest found node. Should no nearby nodes be found the same procedure as for nearby edges, illustrated in \ref{fig:implementation4}, will be executed and the existing edge with which an intersection was found will be split into two edges and the spawned node will be inserted between them and connected to both as well as the spawned edge.\\\\

\begin{figure}[h]
    \centering
    \includegraphics[width=6cm]{figures/intersection_check_node.pdf}
    \caption{Check for nearby nodes}
    \label{fig:implementation3}
\end{figure}

Every production rule contains a parameter called "IntersectRadius" which specifys a circular area around any newly spawned node in which to check for existing nodes or edges. As detailed above in the paragraph regarding edge intersections, should any nodes be found the newly spawned node will be discarded and the spawned edge will instead connect the parent node with the existing node that the check found.\\\\

\begin{figure}[h]
    \centering
    \includegraphics[width=9cm]{figures/intersection_check_edge.pdf}
    \caption{Check for nearby edges}
    \label{fig:implementation4}
\end{figure}

Should no nodes but an existing edge be found within the intersect radius described above, the spawned edge will be extended or shortened so that the spawned node is on the closest found existing edge which then be split into to edges and the node will be inserted between them and connected to both as well as the spawned edge. This procedure is illustrated in \ref{fig:implementation4}.

\section{Output and Collada module}
\label{sec:implementation5}
The system in in it's current form exports the road network as a set of line segments each represented by a simple rectangular polygons. These are created by passing the network created by the L-system trough a geometry generation step. Optionally one may also pass along the height map used and geometry representing it will also be created.\\
The output data is thereafter exported as a Collada file. Collada is an XML-based file format originally created by Sony Computer Entertainment to facilitate importing and exporting data between different software that handle 3D models. Support for it is fairly widespread. Both these properties marked it as a good choice as output format considering the projects purpose as a prototyping tool.\\
Collada is currently under the maintenance of the Khronos Group who also handle maintenance and standardisation of OpenGL. There exist a few libraries for aiding in interaction with Collada files but for this project the official Collada DOM (DOcument Object Model) that is also maintained by the Khronos Group was chosen, partly due to its liberal licensing and partly because of high likelihood of continued development and compliance with future versions of Collada.\\\\

The output form ToshiGen was never intended to be production quality level models but rather a sketch to be refined. 
The system is however designed to allow for relatively easy extension. The representation of the line segments can be changed to any model of arbitrary complexity by modifying or exchanging the geometry generation step. To allow automatic blending for any complex segment models would however require a fair bit of additional work. Some road generation software such as the work of Yamauchi \cite{yamauchi04} go to great lengths to achieve automatic blending of road segments and generation of crossings and the like, including pavements and road markings. It was not considered a priority for this project due to the refinement work that was expected to occur on any generated result that went on to production among other things.\\\\


% Local Variables:
% TeX-master: "00_main.tex"
% End:
