\chapter{Theory}
\label{chap:theory}
The generation of road networks as described herein is driven by an L-system. L-systems are a family of string rewriting systems and essentially constitute a subset of a field of study called \emph{formal languages}. An introduction to these concepts and how they work is in order to facilitate in describing the road generation system.

\section{Formal grammars and languages}
\label{sec:theory1}

Formal language theory, the field concerned with the study of formal languages and grammar is related to applied mathematics trough its relation to formal logic. A \emph{formal grammar} is described as a set of \emph{production rules} for rewriting a string and a starting string, called an \emph{axiom}, from which the rewriting begins. As an example let's consider this simple grammar:
\begin{example}[Formal grammar]
\label{ex:theory1}
     Production rules:\\
     1. S \ensuremath{\rightarrow} aSb\\
     2. S \ensuremath{\rightarrow} ba\\\\
     Starting string (axiom): S\\
     \begin{tabular}{ l l r }
     Step 0: & S & we apply rule 1\\
     Step 1: & aSb & we apply rule 1\\
     Step 2: & aaSbb & we apply rule 1\\
     Step 3: & aaaSbbb & we apply rule 2\\
     Step 4: & aaababbb\\ & \\
     \end{tabular}
\end{example}
We then apply the production rules to the string one at a time. Applying rule '1' to the starting string renders “aSb”. Applying the same rule twice more gives us “aaaSbbb”. Finally applying rule '2' renders us the string “aaababbb”. From this we can easily deduct that applying the production rules in different order and different number of times we can generate an infinite set of strings from this grammar. The total set of strings that can be generated from a formal grammar is the \emph{formal language} of that grammar.

\section{Deterministic and stochastic L-systems}
\label{sec:theory2}

L-systems are generated in the same way a formal language is generated from a formal grammar.  The crucial difference is that whereas in a formal grammar only one production rule is chosen and applied per evolution step, in an L-system as many rules as possible are applied in parallel for each evolution step. To put it in different words, there is no conscious selection of one production to be applied over the entire string but all applicable productions are considered for each and every variable in the string and if there are more than one a mechanism needs to be in place to prioritise between them.\\

This approach has resulted in a plethora of variations of L-systems. The general trend being towards finding ways to apply the most suitable production rule at the right place in the string. As we will see later on in this chapter this has resulted in L-systems moving away from the concept of a “string” to rewrite and towards data structures that are programmatically easier to handle and thus more suited to solving more complex problems.\\

For the time being we will content ourselves with the idea of an L-system as a string rewriting system as demonstrated in section \ref{sec:theory1} on formal grammar and languages above. L-systems are generally described as a \emph{tuple}:\\\\
G = {V, w, P}\\\\
Where 'G' denotes the system, 'V' the vocabulary, or set of \emph{variables} available to the system. 'w' represents the \emph{axiom} which describes the state of the string before the L-system is applied. The axiom is usually a quite simple string but theoretically could be the output from the L-system, or any L-system with the same vocabulary or a subset thereof, as applied to a previous, simpler axiom. Indeed this is what happens at every iteration of  an L-system. Finally 'P' is the set of production rules that determine how the system evolves, for those variables in the vocabulary 'V' that do not have any applicable production rules the identity production “S \ensuremath{\rightarrow} S” is assumed. Such variables are sometimes referred to as \emph{constants} or \emph{terminals}.\\

If we only have one single production rule to match any one variable in the string we have a \emph{deterministic} L-system on our hands. This follows logically as a system like this will produce the same result every time it is executed.\\

L-systems, first detailed in 1968 \cite{lindenmayer68} are the brainchild of theoretical biologist Aristid Lindenmayer (hence “L”-system) and was initially used to simulate the growth of simple multicellular organisms, specifically algae and fungi which were the main focus of Prof. Lindenmayers research at the time.\\

Deterministic L-systems though, soon proved to be quite restrictive. Plants are rarely, if ever, deterministic in their appearance (two trees rarely look exactly alike) and only having a single production rule for each variable severely limits the patterns one is able to generate. Thus the first natural extension to L-systems was the introduction of support for multiple production rules for any variable and, by necessity, a measure of randomness in order to choose between the patterns.\\

In order to illustrate this let us take a second look at the example \ref{ex:theory1}, this time in the terms of an L-system. The vocabulary V consists of {a, b, S} where 'a' and 'b' are constants. The  production rules in 'P' would be {S \ensuremath{\rightarrow} aSb, S \ensuremath{\rightarrow} ba} where the variable on the left of the arrow is called the \emph{predecessor} and the string or variable to the right is, logically, called the \emph{successor}. The element of randomness that warrants the name stochastic L-systems is constituted by attaching a probability factor to each production rule, like so: {S \ensuremath{\rightarrow} aSb : 0.6, S \ensuremath{\rightarrow} ba : 0.4}. The numbers after the colons mean that the first production rule is applied with a probability of 60\% and the second production rule with a probability of 40\%. This addition at the same time allows us to have several production rules that are applicable to the same variable in the string, and to adjust the influence of said production rules in relation to one another. Given that one is using a seeded pseudo-random number generator to choose between the production rules, which is normally the case with implementations of L-systems, one has the dual possibilities of generating infinite variations of the system using different seeds and recreating any system given that one has the proper seed to initialise the random number generator.

\section{The turtle system}
\label{sec:theory3}

Now all of this talk of formal grammars and L-systems is very interesting I'm sure, but just how does it pertain to the generation of computer graphics? The beginning of the answer, and an important link to understanding how L-systems are used and why they have developed the way they have we find in the \emph{turtle system} method of visualising these systems.\\

The Turtle system gets its name from the idea of a turtle crawling along a path while pulling a pen. Wherever the turtle goes it leaves a line behind it. Now imagine having a very well trained turtle which obeys our every instruction. The connection to visualising L-systems becomes apparent if we let these instructions be the sting of symbols generated by an L-system and assign a motion to each symbol. Thus 'F' could be used to represent moving forward one step '+' could symbolise turning say 45\textdegree to the right and '-' turning that same amount to the left.\\

With this set of symbols we could specify L-systems that generate instructions for the turtle system to generate any number of squiggly lines. However, to generate the branching structures we are looking for, both for plants and road networks, we need to introduce symbols that allow the turtle system to save its position before drawing one branch and then return to the saved position in order to draw the next branch, and so on. Usually this function is implemented as a stack of positions where the '[' symbol in a string denotes a push onto said stack and the ']' symbol a pop and return to the stored position.
\begin{example}[Turtle system]
\label{ex:theory2}
    In a turtle system where "F" denotes a movement forward, "+" a positive rotation of 45 degrees and "-" a negative rotation of 45 degrees the figures below correspond to the following strings respectively.\\
    \begin{tabular}{ l l }
    A: & FF\\
    B: & F+FF--F--F++F--F--F\\
    C: & F[+F[+F][-F]][FF][-F[-F][+F]]\\
    \end{tabular}\\\\
    
    \centering
    \includegraphics[width=8cm]{figures/turtle_system.pdf}
\end{example}

\section{Contextual L-systems}
\label{sec:theory4}

Contextual L-systems provide another mechanism for determining the most suitable rule to apply at any one point in the string. By allowing feedback in the form of the data already stored in the string itself, specifically which symbols are present to the direct left and right of the symbol we are considering, i.e. in which "context" the symbol is located. When typing rules this is symbolised by adding hooked brackets, '<' and '>' to the left and/or right of the target symbol with the context(s) to match against on the pointy end of said brackets.
\begin{example}[Contextual replacement rule]
\label{ex:theory3}
    A contexutal replacement rule to replace 'S' with 'bSa' only when 'S' has an 'a' on it's left and a 'b' on it's right would look like so:\\
    a<S>b \ensuremath{\rightarrow} bSa\\\\
    With a randomly selected axiom of: baSba\\
    The first iteration of the system would give us: babSaba\\
    After this point the system cannot be evolved further without adding more rules due to the context constraints specified by the single replacement rule.
\end{example}

\section{Parametric L-systems}
\label{sec:theory5}

The model for L-systems presented so far is fine for creating nice fractal patterns but as we strive to achieve higher levels of realism a problem soon becomes very apparent; lack of information. All instructions to the turtle system are discrete, '+' always rotates the turtle a pre-set amount of degrees and 'F' always moves it a pre-set unit-length forward. Problems with this emerge as soon as we want to create branches or streets with different length and with non-uniform angles between them. Using our current paradigm we could solve this by increasing the vocabulary, thus creating symbols indicating translations and transformations of different length and angles. This however is not only a cumbersome approach but it still limits us to a predetermined set of lengths and angles.\\

To alleviate this problem Hanan \cite{hanan92} introduces the concept of parametric L-systems. In parametric L-systems the symbols are replaced with \emph{modules} that besides the symbol also carry a list of parameters that can specify among other things the extent of the action. When fed to a turtle system these parameters also open up the possibility to associate other data with each movement of the turtle, such as the colour of the line being drawn. A module is constructed from a symbol, for example 'F' that we've used to denote a forward motion of a turtle. This symbol is then combined with a parameter list. The meaning of each parameter are up to the implementation of the L-system and turtle system and thus requires there to be an understanding between the two with regards to this. In this example we might have specified the first parameter to denote the length of the movement and the following three the RGB components of the colour of the line to be drawn.\\

\begin{example}[Module]
\label{ex:theory4}
    A module representing forward movement for a turtle system could be constructed like so:\\
    Symbol: F\\
    Parameter list definition: (length, colour)\\
    Resulting module: F(length, colour)\\

    A module representing a change in rotation of the turtle could be constructed like so:\\
    Symbol: R\\
    Parameter list definition: (angle)\\
    Resulting module: R(angle)\\

    Example string constructed with these modules:\\
    F(1.0, "red") [R(30)F(1.0, "green") [R(30)F(2.0, "orange")] [R(-15)F(1.5, "blue")]] [R(-30)F(1.0, "green") [R(-30)F(2.0, "orange")] [R(15)F(1.5, "blue")]]]\\\\
    The result of which, if fed to the turtle system would look like the figure below.

    \centering
    \includegraphics[width=6cm]{figures/parametric_L-system.pdf}
\end{example}

\section{Allowing loops, an L-system as a graph}
\label{sec:theory6}

Now that we have a basic theoretical understanding of L-systems it's appropriate that we take a closer look at it's applicability for solving the postulated problem and more specifically the theoretical modifications that were necessary to reach an acceptable result.\\

The big, and rather obvious, problem when adapting an algorithm for generating directed, acyclic structures such as trees, to generate decidedly cyclic structures, such as the road networks we are looking to do are loops. The model of L-systems as described so far with a string as the information carrier from step to step in the evolution process presents quite an obstacle here, since allowing for loops would at best require some convoluted mechanism to reference a literal from multiple other literals in the string. Detecting when loops should be created by branches crossing one another would also require us to create some intermediary logical representation of the structure described by the string and the commands to a hypothetical turtle-system contained therein. We would also need to recreate this intermediary structure between each evolutionary step of the L-system as new data is added. Surely there must be a better way.\\

\begin{example}[Problems with intersecting paths]
\label{ex:theory5}
    The figure below shows a L-system that has started to fold in on itself with some particular problem areas marked out. The coloured lines illustrate the origin and direction of the different paths. With a graph-based data structure we have a method of detecting when paths intersect in place we are able to avoid formations that would seem improbable in an actual road network. It is also a much simpler task to adjust the properties of nodes and edges to remedy the problem areas.
    
    \centering
    \includegraphics[width=10cm]{figures/intersection_problem.pdf}
\end{example}

Now, one could argue that detecting these crossing segments is not necessary, that simply allowing the path of the turtle to intersect would suffice in order to generate the visual patterns we're striving for. Alas, this would allow the network of line-segments to fold in on itself removing any sense of control and likely resulting in a far more chaotic appearance than intended. Also, crossings play a central and special part in a road network which further increases the importance of knowing when and where they occur.\\
The conclusion then, would be that we sorely need some other data structure than a string of literals on which we can apply the concepts of an L-system. Preferably this data structure should incorporate the properties of all the L-system variants described above.\\
Lucky for us there is a rather simple and elegant solution to our woes. We can achieve what we want by using a graph. At the same time we also need to let the production rules work with the basic building blocks present in this new context. So instead of symbols in a string we now deal with nodes and edges in a graph. Thus a replacement rule may, instead of replacing a symbol 'S' with a sting '[-FS][FS][+FS]', replace an 'S' type node with three edges with an 'S' node at the end of each. 

\begin{example}[Identical string based and graph based systems]
\label{ex:theory6}
    Let us construct a simple L-system with the intention of feeding the output to a turtle-system. For sake of simplicity let the symbols "+" and "-" imply a positive of negative rotation of 45 degrees and the symbol F imply a forward movement of length 1.0 in the context of the turtle-system. We will let this system iterate 5 times over the string before taking a look at the result.\\\\
    Rule 1: A \ensuremath{\rightarrow} FB\\
    Rule 2: B \ensuremath{\rightarrow} [+A][-A]\\
    Axiom: A\\
    \begin{tabular}{l l}
    Step 0 & A\\
    Step 1 & FB\\
    Step 2 & F[+A][-A]\\
    Step 3 & F[+FB][-FB]\\
    Step 4 & F[+F[+A][-A]][-F[+A][-A]]\\
    Step 5 & F[+F[+FB][-FB]][-F[+FB][-FB]]\\
    \end{tabular}\\\\
    "B" means nothing to the turtle system so for all intents and purposes it sees the above string as equivalent to:\\
    F[+F[+F][-F]][-F[+F][-F]]\\
    
    We will attempt to create the same visual result using a graph based L-system using the following production rules:\\
    \begin{tabular}{l l l | l l l}
    Rule 1: & Match node type: & A   & Rule 2: & Match node type: & B\\
            & Change to type:  & C   &         & Change to type:  & C\\
            & Spawn type:      & B   &         & Spawn type:      & B\\
            & Spawn count:     & 1   &         & Spawn count:     & 2\\
            & Edge length:     & 1.0 &         & Edge length:     & 1.0\\
            &                  &     &         & Edge angle:      & 45\\
    \end{tabular}\\\\
    
    \includegraphics[width=10cm]{figures/graph_based_systems.pdf}\\
    
    As we can see the two production rules from the two systems closely resemble each other and they both produce the same graphical result.
\end{example}

This has the added benefit that we are essentially incorporating the instructions meant for the turtle system into the data structure itself in the form of edges. We are thus evolving a two (or three if we so wish) dimensional graphical representation of the system directly instead of a set of a one dimensional string of instructions on how to draw it. Thereby our problem of creating loops is solved, as it is easy enough to add or detract edges and nodes from one another. The problem with detecting branch overlaps, or near overlaps, in order to create loops also presents a much smaller challenge due to the current state of the system (i.e locations of nodes and edges) being instantly available to us at each evolution step. While there are distinct advantages to this approach it also requires us to expand a bit on the concept of parametric L-systems when it comes to production rules as they now create the edges between nodes and thus need to carry information about how to go about doing that. We may also need additional node types/variables in order to mark nodes as already expanded and avoid it getting matched multiple times. As can be seen in example \ref{ex:theory6} this results in a slightly different approach than with string replacement.

% Local Variables:
% TeX-master: "00_main.tex"
% End:
